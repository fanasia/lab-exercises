Lab04

Point1
a) What the differences about the code and the output from both program?
=============================================================================
The difference about the code is the location of the declaration of varchar.
In global-char, the varchars are declared outside the main, meanwhile in
local-char, the varchar is declared inside the main. The difference output
is in global-char, after the second run, the address remained the same. while,
in local-char, the adress changed in the second run and third run. And also
the global variable address number have a great differences to local variable
adress number.

b) Why Global Variables Address number and Local Variable Address number have
a great difference in their address?
=============================================================================
Because global variables and local variables are stored in different segment.
Global Variables are stored in the data segment, and Local Variables are
stored in the stack segment.
References : stackoverflow.com/questions/21657998/address-of-local-and-global-
vaiables

c)Write your knowledge about Global Variable Address and Local Variable
Address
============================================================================
Local Variables only exist inside the specific function that creates them.
As such, they are normally implemented using a stack, and unknown to other
functions and to the main program. They are recreated each time a function
is executed or called, that's why the address keep changing after each
different run.
Global Variables can be accessed by any function in the program. They are
implemented by associating memory locations with variable names.

Point2
a) Explain the parameter of flag in open function
O_CREAT | O_RDWR =  If the files doesn't exist, create it. But if it exists, the O_CREATE has no effect.
And also after created, open the file so that it can be read from and written to.  

b) Explain the parameter of modes in open function
S_IRWXU : The owner has read, write and execute permission
S_IRWXU|S_IRGRP|S_IWGRP|S_IROTH : : The owner has r,w,x permission, group has read permission, group has write
permission, and others have read permission.
0711 : Change into binary mode. 110-001-001. It means Owner has read and write permission, group only has execute
permission, and also others only has execute permission.
0700 : Change into binary mode. 110-000-000. It means owner has read and write permission, while group and other
do not have any permission. 

c) Write your knowledge about open and close I/O process
Data is stored in file. Open and close function lets you open and close a file. It also lets you create the
file if not exist and set the mode of file for owner, group and others.

Point3
a) Write your knowlede about write program
int  write(  int  handle,  void  *buffer,  int  nbyte  );
The write() function attempts to write nbytes from buffer to the file associated with handle. On text files, it expands each LF to a CR/LF.It return the number of bytes written to the file. A return value of -1 indicates an error, with errno set appropriately.
References : http://webcache.googleusercontent.com/search?q=cache:ftp://gd.tuwien.ac.at/languages/c/programming-bbrown/c_075.htm

